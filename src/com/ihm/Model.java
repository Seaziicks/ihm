package com.ihm;

public class Model {

    public static final float ANGLE_MAX_VALUE = 15.0f;
    public static final float ANGLE_MIN_VALUE = 5.0f;
    public static final float ANGLE_INITIAL_VALUE = 12.0f;

    private PossibleState currentState;
    float angleValue;
    private boolean angleValueWasOutOfRange;
    private Controller controller;

    public Model() {
        this.currentState = this.getInitialState();
    }

    void goToNextState() {
        switch (this.getCurrentState()) {
            case AUTO:
                this.currentState = PossibleState.MANUAL;
                break;
            case STAB_ON:
                this.currentState = PossibleState.AUTO;
                break;
            case STAB_OFF:
                this.currentState = PossibleState.STAB_ON;
                break;
            case MANUAL:
                this.currentState = PossibleState.STAB_OFF;
                break;
            default:
                throw new AssertionError();
        }
    }

    public PossibleState getCurrentState() {
        return currentState;
    }

    void setAngleValue(float angleValue) {
        if (angleValue > ANGLE_MAX_VALUE) {
            this.angleValue = ANGLE_MAX_VALUE;
            this.controller.setAngleValueOutOfRange(this.angleValue);
            this.angleValueWasOutOfRange = true;
        }
        else if (angleValue < ANGLE_MIN_VALUE) {
            this.angleValue = ANGLE_MIN_VALUE;
            this.controller.setAngleValueOutOfRange(this.angleValue);
            this.angleValueWasOutOfRange = true;
        }
        else {
            this.angleValue = angleValue;
            this.controller.setAngleValueInRange(this.angleValue);
            this.angleValueWasOutOfRange = false;
        }
    }

    void setAngleValueReminder() {
        if (angleValueWasOutOfRange) {
            this.controller.reminderAngleValueOutOfRange();
        }
        else {
            this.controller.reminderAngleValueInRange();
        }
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public PossibleState getInitialState() {
        return PossibleState.AUTO;
    }
}
