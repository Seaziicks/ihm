package com.ihm;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.*;
import java.awt.*;
import java.nio.file.FileSystems;

public class View {

    private Controller controller;

    private JButton buttonManual, buttonStabOff, buttonStabOn, buttonSelectAuto;
    private JButton wxon, tst, wxa, stdby, off;
    private JTextField angleTextField;


    public View() {
        this.createUI();
    }

    /**
     * Permet de créer la vue en entière, à la fois la partie gauche gérant le mode via {@link View#modePanel()}, et la
     * partie droite gérant la valeur de l'angle via {@link View#anglePanel()}.
     */
    void createUI() {
        JFrame frame = new JFrame("IHM");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(488,600));
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        JPanel panel = new JPanel(new GridLayout(1, 2));
        JPanel anglePanel = this.modePanel();

        JPanel modePanel = this.anglePanel();

        frame.setBackground(new Color(41, 37, 37));
        anglePanel.setBackground(new Color(41, 37, 37));
        modePanel.setBackground(new Color(41, 37, 37));

        panel.add(anglePanel);
        panel.add(modePanel);

        frame.setContentPane(panel);
        String[] pathNames = { "com", "ihm", "resources", "plane.png"};
        String path = String.join(FileSystems.getDefault().getSeparator(), pathNames);
        java.net.URL url = ClassLoader.getSystemResource(path);
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.createImage(url);
        frame.setIconImage(img);
        frame.pack();
        frame.repaint();
    }

    /**
     * Permet de générer le panel de gauche, qui contiendra les boutons pour changer de mode.
     * @return JPanel   Le JPanel contenant la gestion du mode de WXR.
     */
    private JPanel modePanel() {
        JPanel anglePanel = new JPanel(new BorderLayout());
        JTextPane title = new JTextPane();
        Style defaut = title.getStyle("default");
        title.setPreferredSize(new Dimension(25,25));
        title.setBackground(new Color(120, 136, 192));
        title.setEnabled(false);
        title.setDisabledTextColor(Color.BLACK);
        try {
            title.getDocument().insertString(0, "MOD", defaut);
            StyledDocument doc = title.getStyledDocument();
            SimpleAttributeSet center = new SimpleAttributeSet();
            StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
            doc.setParagraphAttributes(0, doc.getLength(), center, false);
        } catch (BadLocationException ignored) { }
        anglePanel.add(title, BorderLayout.NORTH);


        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.NONE;


        this.wxon = this.getRoudedButton("WXON", 100, 150);
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(3,0,30,30);
        this.wxon.setBackground(new Color(113, 195, 99));
        buttonPanel.add(this.wxon, c);


        this.tst = this.getRoudedButton("TST", 100, 150);
        c.gridx = 5;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(3,3,3,0);
        tst.setBackground(this.getEnabledButtonBackgroundColor());
        buttonPanel.add(tst, c);


        this.wxa = this.getRoudedButton("WXA", 100, 150);
        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(3,0,30,3);
        wxa.setBackground(this.getEnabledButtonBackgroundColor());
        buttonPanel.add(wxa, c);


        this.stdby = this.getRoudedButton("STDBY", 100, 150);
        c.gridx = 5;
        c.gridy = 5;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(3,3,3,0);
        stdby.setBackground(this.getEnabledButtonBackgroundColor());
        buttonPanel.add(stdby, c);


        this.off = this.getRoudedButton("OFF", 100, 150);
        c.gridx = 0;
        c.gridy = 10;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.insets = new Insets(3,0,3,3);
        off.setBackground(this.getEnabledButtonBackgroundColor());
        buttonPanel.add(off, c);
        buttonPanel.setBackground(new Color(41, 37, 37));

        this.wxon.addActionListener( e -> this.controller.onClickWxon());
        this.tst.addActionListener( e -> this.controller.onClickTst());
        this.wxa.addActionListener( e -> this.controller.onClickWxa());
        this.stdby.addActionListener( e -> this.controller.onClickStdby());
        this.off.addActionListener( e -> this.controller.onClickOff());


        anglePanel.add(buttonPanel, BorderLayout.CENTER);
        anglePanel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 3, Color.WHITE));

        return anglePanel;
    }

    /**
     * Permet de générer le panel de droite, qui contiendra la partie servant à définir l'angle du WXR.
     * @return JPanel   Le JPanel contenant la gestion de l'angle du WXR.
     */
    private JPanel anglePanel() {
        JPanel modePanel = new JPanel(new BorderLayout());
        JTextPane title = new JTextPane();
        title.setPreferredSize(new Dimension(25,25));
        title.setBackground(new Color(120, 136, 192));
        Style defaut = title.getStyle("default");
        title.setEnabled(false);
        title.setDisabledTextColor(Color.BLACK);
        try {
            title.getDocument().insertString(0, "ANGLE", defaut);
            StyledDocument doc = title.getStyledDocument();
            SimpleAttributeSet center = new SimpleAttributeSet();
            StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
            doc.setParagraphAttributes(0, doc.getLength(), center, false);
        } catch (BadLocationException ignored) { }
        modePanel.add(title, BorderLayout.NORTH);


        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.VERTICAL;

        // JPanel buttonPanel = new JPanel(new GridLayout(5,1));

        this.buttonManual = getRoudedButton("MANUAL", 200, 50);
        this.buttonManual.setEnabled(true);
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 4;
        c.gridheight = 4;
        c.insets = new Insets(15,0,15,0);
        buttonPanel.add(buttonManual, c);

        this.buttonStabOff = getRoudedButton("STAB OFF", 200, 50);
        this.buttonStabOff.setEnabled(false);
        c.gridx = 0;
        c.gridy = 10;
        c.gridwidth = 4;
        c.gridheight = 4;
        c.insets = new Insets(15,0,15,0);
        buttonPanel.add(buttonStabOff, c);

        this.angleTextField = new JTextField();
        this.angleTextField.setEnabled(false);
        this.angleTextField.setHorizontalAlignment(JTextField.CENTER);
        this.angleTextField.setDisabledTextColor(Color.BLACK);
        this.angleTextField.setFont(this.angleTextField.getFont().deriveFont(Font.BOLD, 14f));
        this.angleTextField.setPreferredSize(new Dimension(200, 150));
        c.gridx = 0;
        c.gridy = 20;
        c.gridwidth = 4;
        c.gridheight = 4;
        c.insets = new Insets(15,0,15,0);
        buttonPanel.add(angleTextField, c);

        this.buttonStabOn = getRoudedButton("STAB ON", 200, 50);
        this.buttonStabOn.setEnabled(false);
        c.gridx = 0;
        c.gridy = 30;
        c.gridwidth = 4;
        c.gridheight = 4;
        c.insets = new Insets(15,0,15,0);
        buttonPanel.add(buttonStabOn, c);

        this.buttonSelectAuto = getRoudedButton("SELECT AUTO", 200, 50);
        this.buttonSelectAuto.setEnabled(false);
        c.gridx = 0;
        c.gridy = 40;
        c.gridwidth = 4;
        c.gridheight = 4;
        c.insets = new Insets(15,0,15,0);
        buttonPanel.add(buttonSelectAuto, c);

        buttonPanel.setBackground(new Color(41, 37, 37));
        modePanel.add(buttonPanel, BorderLayout.CENTER);
        modePanel.setBackground(new Color(41, 37, 37));

        buttonManual.addActionListener(evt -> controller.onClickButtonManual());
        buttonStabOff.addActionListener(evt -> controller.onClickButtonStabOff());
        buttonStabOn.addActionListener(evt -> controller.onClickButtonStabOn());
        buttonSelectAuto.addActionListener(evt -> controller.onClickButtonSelectAuto());


        return modePanel;
    }

    /**
     * Bouton spécial ayant des bordures arrondies.
     * @param text  Le texte à mettre dans le bouton
     * @param width     La longueur du bouton
     * @param height    La hauteur du bouton
     * @return  JButton     Retourne le bouton avec bord arrondis
     */
    private JButton getRoudedButton(String text, int width, int height) {
        JButton roundedBorderButton = new JButton(text);
        roundedBorderButton.setBorder(new RoundedBorder(10));
        roundedBorderButton.setPreferredSize(new Dimension(width,height));

        return  roundedBorderButton;
    }

    /**
     * Classe pour avoir des boutons avec une rouded bordure.
     */
    private static class RoundedBorder implements Border {
        private final int radius;
        RoundedBorder(int radius) {
            this.radius = radius;
        }
        public Insets getBorderInsets(Component c) {
            return new Insets(this.radius+1, this.radius+1, this.radius+2, this.radius);
        }
        public boolean isBorderOpaque() {
            return true;
        }
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.drawRoundRect(x, y, width-1, height-1, radius, radius);
        }
    }

    /**
     * Permet d'obtenir la couleur d'arrière-plan d'un bouton désactivé.
     * @return Color    La couleur d'arrière-plan d'un bouton désactivé.
     */
    private Color getDisabledButtonBackgroundColor() {
        return new Color(111, 111, 111);
    }

    /**
     * Permet d'obtenir la couleur d'arrière-plan d'un bouton activé.
     * @return  Color   La couleur d'arrière-plan d'un bouton activé.
     */
    private Color getEnabledButtonBackgroundColor() {
        return new Color(255, 255, 255);
    }

    /**
     * Permet d'obtenir la couleur d'arrière plan d'un bouton seléctionné.
     * @return  Color   La couleur d'arrière plan d'un bouton seléctionné.
     */
    private Color getSelectedButtonBackgroundColor() {
        return new Color(113, 195, 99);
    }

    /**
     * Permet de changer l'activation et la couleur du bakcground du bouton MANUAL, en fonction de son état d'activation.
     * @param buttonState   L'état du bouton souhaité (activé ou désactivé)
     */
    void setButtonManualState(boolean buttonState) {
        this.buttonManual.setEnabled(buttonState);

        Color c = buttonState ? this.getEnabledButtonBackgroundColor() : this.getDisabledButtonBackgroundColor();
        this.buttonManual.setBackground(c);
    }
    /**
     * Permet de changer l'activation et la couleur du bakcground du bouton STABB_OFF, en fonction de son état d'activation.
     * @param buttonState   L'état du bouton souhaité (activé ou désactivé)
     */
    void setButtonStabOffState(boolean buttonState) {
        this.buttonStabOff.setEnabled(buttonState);

        Color c = buttonState ? this.getEnabledButtonBackgroundColor() : this.getDisabledButtonBackgroundColor();
        this.buttonStabOff.setBackground(c);
    }
    /**
     * Permet de changer l'activation et la couleur du bakcground du textField de l'angle, en fonction de son état d'activation.
     * @param textfFeldState   L'état du textfield souhaité (activé ou désactivé)
     */
    void setAngleTextFieldState(boolean textfFeldState) {
        this.angleTextField.setEnabled(textfFeldState);

        Color c = textfFeldState ? this.getEnabledButtonBackgroundColor() : this.getDisabledButtonBackgroundColor();
        this.angleTextField.setBackground(c);

        if (textfFeldState) {
            this.angleTextField.requestFocusInWindow();
            this.angleTextField.selectAll();
        }
    }
    /**
     * Permet de changer l'activation et la couleur du bakcground du bouton STAB_ON, en fonction de son état d'activation.
     * @param buttonState   L'état du bouton souhaité (activé ou désactivé)
     */
    void setButtonStabOnState(boolean buttonState) {
        this.buttonStabOn.setEnabled(buttonState);

        Color c = buttonState ? this.getEnabledButtonBackgroundColor() : this.getDisabledButtonBackgroundColor();
        this.buttonStabOn.setBackground(c);
    }
    /**
     * Permet de changer l'activation et la couleur du bakcground du bouton OFF, en fonction de son état d'activation.
     * @param buttonState   L'état du bouton souhaité (activé ou désactivé)
     */
    void setButtonSelectAutoState(boolean buttonState) {
        this.buttonSelectAuto.setEnabled(buttonState);

        Color c = buttonState ? this.getEnabledButtonBackgroundColor() : this.getDisabledButtonBackgroundColor();
        this.buttonSelectAuto.setBackground(c);
    }

    /**
     * Permet de récupérer la valeur de l'angle du textField.
     * @return  String La valeur de l'angle.
     */
    String getAngleValue() {
        return this.angleTextField.getText();
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * Permet de mettre à jour la vue avec une nouvelle valeur de l'angle.
     * @param angleValue    La valeur de l'angle.
     */
    void setAngleValue(String angleValue) {
        this.angleTextField.setText(angleValue);
    }

    /**
     * Permet de notifier l'utilisateur que la valeur de l'angle n'est pas bonne.
     * Cela peut arriver quand la valeur de l'angle dépasse la borne supérieure {@link com.ihm.Model#ANGLE_MAX_VALUE}.
     * Ou quand la valeur de l'angle ets inférieure à {@link com.ihm.Model#ANGLE_MIN_VALUE}.
     */
    void angleValueOutOfRange() {
        this.angleTextField.setBackground(new Color(223, 62, 62));
        this.angleTextField.setDisabledTextColor(Color.BLACK);
    }

    /**
     * Permet de notifier l'utilisateur que la valeur de l'angle est bonne.
     * Bonne veut dire comprise entre {@link com.ihm.Model#ANGLE_MIN_VALUE} et {@link com.ihm.Model#ANGLE_MAX_VALUE}.
     */
    void angleValueInRange() {
        this.angleTextField.setBackground(new Color(113, 195, 99));
        this.angleTextField.setDisabledTextColor(Color.BLACK);
    }

    /**
     * Permet de notifier l'utilisateur que la valeur précédente qu'il a fourni au système n'était pas bonne.
     */
    void reminderAngleValueOutOfRange() {
        this.angleTextField.setDisabledTextColor(new Color(255, 87, 51));
    }

    /**
     * Permet de notfier l'utilisateur que la valeur précédente qu'il a fourni au système était bonne.
     */
    void reminderAngleValueInRange() {
        this.angleTextField.setDisabledTextColor(new Color(113, 195, 99));
    }

    /**
     * Permet de changer de fond du bouton WXON, en fonction de son état.
     * @param boobool   Booléen permettant de savoir si le bouton est seléctionné ou non.
     */
    void setButtonWxon(boolean boobool) {
        this.wxon.setBackground(boobool ? this.getSelectedButtonBackgroundColor() : this.getEnabledButtonBackgroundColor());
    }
    /**
     * Permet de changer de fond du bouton TST, en fonction de son état.
     * @param boobool   Booléen permettant de savoir si le bouton est seléctionné ou non.
     */
    void setButtonTst(boolean boobool) {
        this.tst.setBackground(boobool ? this.getSelectedButtonBackgroundColor() : this.getEnabledButtonBackgroundColor());
    }
    /**
     * Permet de changer de fond du bouton WXA, en fonction de son état.
     * @param boobool   Booléen permettant de savoir si le bouton est seléctionné ou non.
     */
    void setButtonWxa(boolean boobool) {
        this.wxa.setBackground(boobool ? this.getSelectedButtonBackgroundColor() : this.getEnabledButtonBackgroundColor());
    }
    /**
     * Permet de changer de fond du bouton STDBY, en fonction de son état.
     * @param boobool   Booléen permettant de savoir si le bouton est seléctionné ou non.
     */
    void setButtonStdby(boolean boobool) {
        this.stdby.setBackground(boobool ? this.getSelectedButtonBackgroundColor() : this.getEnabledButtonBackgroundColor());
    }
    /**
     * Permet de changer de fond du bouton OFF, en fonction de son état.
     * @param boobool   Booléen permettant de savoir si le bouton est seléctionné ou non.
     */
    void setButtonOff(boolean boobool) {
        this.off.setBackground(boobool ? this.getSelectedButtonBackgroundColor() : this.getEnabledButtonBackgroundColor());
    }
}

