package com.ihm;

public enum PossibleState {
    AUTO(true, false, false, false, false),
    MANUAL(false, true, false, false, false),
    STAB_OFF(false, false, true, true, false),
    STAB_ON(false, false, false, false, true);

    private final boolean buttonManualEnabled;
    private final boolean buttonStabOffEnabled;
    private final boolean angleTextModificationEnabled;
    private final boolean buttonStabOnEnabled;
    private final boolean buttonSelectAutoEnabled;

    PossibleState(boolean buttonManualEnabled, boolean buttonStabOffEnabled, boolean angleTextModificationEnabled, boolean buttonStabOnEnabled, boolean buttonSelectAutoEnabled) {
        this.buttonManualEnabled = buttonManualEnabled;
        this.buttonStabOffEnabled = buttonStabOffEnabled;
        this.angleTextModificationEnabled = angleTextModificationEnabled;
        this.buttonStabOnEnabled = buttonStabOnEnabled;
        this.buttonSelectAutoEnabled = buttonSelectAutoEnabled;
    }

    public boolean getButtonManualEnabled() {
        return buttonManualEnabled;
    }

    public boolean getButtonStabOffEnabled() {
        return buttonStabOffEnabled;
    }

    public boolean getAngleTextModificationEnabled() {
        return angleTextModificationEnabled;
    }

    public boolean getButtonStabOnEnabled() {
        return buttonStabOnEnabled;
    }

    public boolean getButtonSelectAutoEnabled() {
        return buttonSelectAutoEnabled;
    }
}
