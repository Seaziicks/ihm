package com.ihm;

public class Controller {

    private final View view;
    private final Model model;

    public Controller() {
        this.model = new Model();
        this.view = new View();
    }

    void onClickButtonManual() {
        switch (this.model.getCurrentState()) {
            case AUTO:
                this.model.goToNextState();
                this.updateView(this.model.getCurrentState());
                break;
            case STAB_ON:
            case STAB_OFF:
            case MANUAL:
                throw new IllegalStateException();
            default:
                throw new AssertionError();
        }
    }

    void onClickButtonStabOff() {
        switch (this.model.getCurrentState()) {
            case MANUAL:
                this.model.goToNextState();
                this.updateView(this.model.getCurrentState());
                break;
            case AUTO:
            case STAB_ON:
            case STAB_OFF:
                throw new IllegalStateException();
            default:
                throw new AssertionError();
        }
    }

    void onClickButtonStabOn() {
        switch (this.model.getCurrentState()) {
            case STAB_OFF:
                this.model.goToNextState();
                this.updateView(this.model.getCurrentState());
                break;
            case MANUAL:
            case AUTO:
            case STAB_ON:
                throw new IllegalStateException();
            default:
                throw new AssertionError();
        }
    }

    void onClickButtonSelectAuto() {
        switch (this.model.getCurrentState()) {
            case STAB_ON:
                this.model.goToNextState();
                this.updateView(this.model.getCurrentState());
                break;
            case STAB_OFF:
            case MANUAL:
            case AUTO:
                throw new IllegalStateException();
            default:
                throw new AssertionError();
        }
    }

    void onClickWxon() {
        this.display(MODES.WXON);
    }

    void onClickTst() {
        this.display(MODES.TST);
    }
    void onClickWxa() {
        this.display(MODES.WXA);
    }
    void onClickStdby() {
        this.display(MODES.STDBY);
    }
    void onClickOff() {
        this.display(MODES.OFF);
    }

    void display(MODES mode) {
        switch (mode) {
            case WXON:
                this.view.setButtonWxon(true);
                this.view.setButtonTst(false);
                this.view.setButtonWxa(false);
                this.view.setButtonStdby(false);
                this.view.setButtonOff(false);
                break;
            case TST:
                this.view.setButtonWxon(false);
                this.view.setButtonTst(true);
                this.view.setButtonWxa(false);
                this.view.setButtonStdby(false);
                this.view.setButtonOff(false);
                break;
            case WXA:
                this.view.setButtonWxon(false);
                this.view.setButtonTst(false);
                this.view.setButtonWxa(true);
                this.view.setButtonStdby(false);
                this.view.setButtonOff(false);
                break;
            case STDBY:
                this.view.setButtonWxon(false);
                this.view.setButtonTst(false);
                this.view.setButtonWxa(false);
                this.view.setButtonStdby(true);
                this.view.setButtonOff(false);
                break;
            case OFF:
                this.view.setButtonWxon(false);
                this.view.setButtonTst(false);
                this.view.setButtonWxa(false);
                this.view.setButtonStdby(false);
                this.view.setButtonOff(true);
                break;
        }
    }

    public void init() {
        this.view.setController(this);
        this.model.setController(this);

        this.model.setAngleValue(Model.ANGLE_INITIAL_VALUE);
        this.updateView(this.model.getCurrentState());
    }

    public void updateView(PossibleState state) {
        this.view.setButtonManualState(state.getButtonManualEnabled());
        this.view.setButtonStabOffState(state.getButtonStabOffEnabled());
        this.view.setAngleTextFieldState(state.getAngleTextModificationEnabled());
        this.view.setButtonStabOnState(state.getButtonStabOnEnabled());
        this.view.setButtonSelectAutoState(state.getButtonSelectAutoEnabled());

        if (state.getButtonSelectAutoEnabled()) {
            this.model.setAngleValue(this.getAngleValueFromView());
        } else if (state.getButtonManualEnabled()) {
            this.model.setAngleValueReminder();
        }
    }

    float getAngleValueFromView() {
        return Float.parseFloat(this.view.getAngleValue().replace(',', '.'));
    }

    void setAngleValueOutOfRange(float angleValue) {
        this.view.setAngleValue(Float.toString(angleValue));
        this.view.angleValueOutOfRange();
    }

    void setAngleValueInRange(float angleValue) {
        this.view.setAngleValue(Float.toString(angleValue));
        this.view.angleValueInRange();
    }

    void reminderAngleValueOutOfRange() {
        this.view.reminderAngleValueOutOfRange();
    }

    void reminderAngleValueInRange() {
        this.view.reminderAngleValueInRange();
    }

    private enum MODES {
        WXON, TST, WXA, STDBY, OFF
    }
}
